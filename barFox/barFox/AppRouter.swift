// AppRouter.swift
// Copyright © RoadMap. All rights reserved.

import Swinject

/// Создание контейнера
final class AppRouter {
    // MARK: - Public methods

    static func makeDrinkCatalogViewController() -> Container {
        let container = Container()
        container.register(NetworkServicable.self) { _ in
            NetworkService()
        }
        container.register(PhotoServicable.self) { _ in
            PhotoService()
        }
        container.register(CocktailsTableViewController.self) { resolve in
            let cocktailsTableViewController = CocktailsTableViewController(
                networkService: resolve.resolve(NetworkServicable.self),
                photoService: resolve.resolve(PhotoServicable.self)
            )
            return cocktailsTableViewController
        }
        return container
    }

    static func makeDrinkIngredientsViewController(drinkID: String?) -> Container {
        let container = Container()
        container.register(NetworkServicable.self) { _ in
            NetworkService()
        }
        container.register(PhotoServicable.self) { _ in
            PhotoService()
        }
        container.register(IngridientsViewController.self) { resolve in
            let drinkIngredientsViewController = IngridientsViewController(
                networkService: resolve.resolve(NetworkServicable.self),
                photoService: resolve.resolve(PhotoServicable.self),
                drinkID: drinkID
            )
            return drinkIngredientsViewController
        }
        return container
    }
}
