// IngridientsResponse.swift
// Copyright © RoadMap. All rights reserved.

import Foundation

/// Результат запроса на получения ингридиентов напитка
struct IngridientsResponse: Decodable {
    /// Ингредиенты
    let drinks: [Ingredients]
}
