// CocktailsInfo.swift
// Copyright © RoadMap. All rights reserved.

import Foundation

/// Информация о напитках
struct CocktailsInfo: Decodable {
    /// Идентификатор  напитка
    let drinkID: String
    /// Имя напитка
    let drinkName: String
    /// Фото  напитка
    let photoNameURL: String

    enum CodingKeys: String, CodingKey {
        case drinkID = "idDrink"
        case drinkName = "strDrink"
        case photoNameURL = "strDrinkThumb"
    }
}
