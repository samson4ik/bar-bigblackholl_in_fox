// SceneDelegate.swift
// Copyright © RoadMap. All rights reserved.

import UIKit

/// Scene Delegate
class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    // MARK: - Public Properties

    var window: UIWindow?

    // MARK: - Public methods

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        guard let windowScene = scene as? UIWindowScene else { return }
        let window = UIWindow(windowScene: windowScene)
        let container = AppRouter.makeDrinkCatalogViewController()
        let navigationController = UINavigationController(
            rootViewController: container
                .resolve(CocktailsTableViewController.self) ?? UINavigationController()
        )
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        window.backgroundColor = .white
        self.window = window
    }
}
