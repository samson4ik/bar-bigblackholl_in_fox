// PhotoService.swift
// Copyright © RoadMap. All rights reserved.

import Alamofire
import UIKit

/// Запрос в сеть для получения фото напитков
final class PhotoService: PhotoServicable {
    func fetchImage(imageURL: String, completion: @escaping (Result<Data, Error>) -> ()) {
        guard let url = URL(string: imageURL) else { return }
        AF.request(url).responseData { response in
            switch response.result {
            case let .success(data):
                completion(.success(data))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}
