// NetworkServicable.swift
// Copyright © RoadMap. All rights reserved.

import Foundation

/// Протокол с функциями запроса для получения названия напитков и их ингридиентов
protocol NetworkServicable {
    func fetchCocktails(completion: @escaping (Result<CocktailsResponse, Error>) -> ())
    func fetchIngridients(id: String, completion: @escaping (Result<IngridientsResponse, Error>) -> ())
}
