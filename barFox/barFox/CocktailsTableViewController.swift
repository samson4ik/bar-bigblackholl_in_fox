// CocktailsTableViewController.swift
// Copyright © RoadMap. All rights reserved.

import UIKit

/// Экран с фото и названием напитка
final class CocktailsTableViewController: UITableViewController {
    // MARK: - Constants

    private enum Constants {
        static let fatalErrorText = "init(coder:) has not been implemented"
        static let cellID = "cell"
    }

    // MARK: - Private properties

    private let networkService: NetworkServicable?
    private let photoService: PhotoServicable?
    private var cocktails: [CocktailsInfo] = []

    // MARK: - Init

    init(networkService: NetworkServicable?, photoService: PhotoServicable?) {
        self.networkService = networkService
        self.photoService = photoService
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError(Constants.fatalErrorText)
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
    }

    // MARK: - Private Methods

    private func setTableView() {
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constants.cellID)
        fetchCocktails()
    }

    private func fetchCocktails() {
        networkService?.fetchCocktails { [weak self] result in
            guard let self else { return }
            switch result {
            case let .success(result):
                self.cocktails = result.drinks
                self.tableView.reloadData()
            case let .failure(error):
                print(error.localizedDescription)
            }
        }
    }

    private func fetchNameAndPhotoCell(url: String, drinkName: String, cell: UITableViewCell) {
        photoService?.fetchImage(imageURL: url) { result in
            switch result {
            case let .success(data):
                cell.textLabel?.text = drinkName
                cell.imageView?.image = UIImage(data: data)
            case let .failure(error):
                print(error.localizedDescription)
            }
        }
    }
}

// MARK: - UITableViewDataSource

extension CocktailsTableViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cocktails.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cellID)
        else { return UITableViewCell() }
        let cocktails = cocktails[indexPath.row]
        fetchNameAndPhotoCell(url: cocktails.photoNameURL, drinkName: cocktails.drinkName, cell: cell)
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cocktails = cocktails[indexPath.row]
        let container = AppRouter.makeDrinkIngredientsViewController(drinkID: cocktails.drinkID)
        guard let ingredientViewController = container.resolve(IngridientsViewController.self) else { return }
        ingredientViewController.title = cocktails.drinkName
        ingredientViewController.drinkID = cocktails.drinkID
        navigationController?.pushViewController(ingredientViewController, animated: true)
    }
}
