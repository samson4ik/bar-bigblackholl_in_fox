// NetworkService.swift
// Copyright © RoadMap. All rights reserved.

import Alamofire
import Foundation

/// Сетевой сервис коктейлей
final class NetworkService: NetworkServicable {
    // MARK: - Constants

    private enum Constants {
        static let baseURL = "https://www.thecocktaildb.com/api/json/v1/1/"
        static let filter = "filter.php"
        static let lookup = "lookup.php"
        static let cText = "c"
        static let name = "Cocktail"
        static let idText = "i"
    }

    // MARK: - Public Methods

    func fetchCocktails(completion: @escaping (Result<CocktailsResponse, Error>) -> ()) {
        let parameters: Parameters = [
            Constants.cText: Constants.name
        ]
        let path = "\(Constants.baseURL)\(Constants.filter)"
        AF.request(path, parameters: parameters).responseData { response in
            guard let data = response.data else { return }
            do {
                let request = try JSONDecoder().decode(CocktailsResponse.self, from: data)
                completion(.success(request))
            } catch {
                completion(.failure(error))
            }
        }
    }

    func fetchIngridients(id: String, completion: @escaping (Result<IngridientsResponse, Error>) -> ()) {
        let parameters: Parameters = [
            Constants.idText: id
        ]
        let path = "\(Constants.baseURL)\(Constants.lookup)"
        AF.request(path, parameters: parameters).responseData { response in
            guard let data = response.data else { return }
            do {
                let request = try JSONDecoder().decode(IngridientsResponse.self, from: data)
                completion(.success(request))
            } catch {
                completion(.failure(error))
            }
        }
    }
}
