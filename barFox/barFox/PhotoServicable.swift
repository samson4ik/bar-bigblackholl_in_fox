// PhotoServicable.swift
// Copyright © RoadMap. All rights reserved.

import Foundation

/// Протокол с функцией запроса для получения фото напитков
protocol PhotoServicable {
    func fetchImage(imageURL: String, completion: @escaping (Result<Data, Error>) -> ())
}
