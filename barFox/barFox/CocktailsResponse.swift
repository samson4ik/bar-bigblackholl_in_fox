// CocktailsResponse.swift
// Copyright © RoadMap. All rights reserved.

import Foundation

/// Результат запроса на получения напитков
struct CocktailsResponse: Decodable {
    /// Напитки
    let drinks: [CocktailsInfo]
}
