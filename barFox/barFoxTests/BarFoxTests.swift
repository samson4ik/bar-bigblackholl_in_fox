// BarFoxTests.swift
// Copyright © RoadMap. All rights reserved.

import Alamofire
@testable import barFox
import XCTest

/// Тесты
final class RoadMapCocktailBarTests: XCTestCase {
    // MARK: - Constants

    private enum Constants {
        static let fakeID = "1"
        static let fakeDrinkName = "Apcent"
        static let emptyText = ""
        static let vodkaText = "Vodka"
        static let lemonText = "Lemon"
    }

    // MARK: - Private properties

    private let drink = CocktailsResponse(
        drinks: [CocktailsInfo(
            drinkID: Constants.fakeID,
            drinkName: Constants.fakeDrinkName,
            photoNameURL: Constants.emptyText
        )]
    )
    private let ingredients = IngridientsResponse(
        drinks:
        [Ingredients(
            cocktailName: Constants.vodkaText,
            cocktailID: Constants.fakeID,
            ingredientOne: Constants.lemonText,
            ingredientTwo: Constants.vodkaText,
            ingredientThree: nil,
            ingredientFour: nil,
            ingredientFive: nil,
            ingredientSix: nil,
            ingredientSeven: nil,
            ingredientEight: nil,
            ingredientNine: nil,
            ingredientTen: nil,
            ingredientEleven: nil,
            ingredientTwelve: nil,
            ingredientThirteen: nil,
            ingredientFourteen: nil,
            ingredientFifteen: nil
        )]
    )

    private var networkServiceProtocol: NetworkServicable?
    private var id = Constants.fakeID

    // MARK: - Public Methods

    override func tearDown() {
        networkServiceProtocol = nil
    }

    func testCocktail() {
        networkServiceProtocol = MockNetworkService(drinks: drink)
        XCTAssertNotNil(networkServiceProtocol)
        var drinks: CocktailsResponse?
        networkServiceProtocol?.fetchCocktails { result in
            switch result {
            case let .success(drink):
                drinks = drink
            case let .failure(error):
                print(error.localizedDescription)
            }
        }
        XCTAssertNotEqual(drinks?.drinks.count, 0)
    }

    func testIngredient() {
        networkServiceProtocol = MockNetworkService(ingredients: ingredients)
        XCTAssertNotNil(networkServiceProtocol)
        var ingredients: IngridientsResponse?
        networkServiceProtocol?.fetchIngridients(id: id) { result in
            switch result {
            case let .success(ingredientsInfo):
                ingredients = ingredientsInfo
            case let .failure(error):
                print(error.localizedDescription)
            }
        }
        XCTAssertNotEqual(ingredients?.drinks.count, 0)
    }
}

/// Мок сетевого сервиса
final class MockNetworkService: NetworkServicable {
    // MARK: Constants

    private enum Constants {
        static let emptyText = ""
    }

    // MARK: - Private properties

    private var drinks: CocktailsResponse?
    private var ingredients: IngridientsResponse?

    // MARK: - Init

    init() {}

    convenience init(drinks: CocktailsResponse) {
        self.init()
        self.drinks = drinks
    }

    convenience init(ingredients: IngridientsResponse) {
        self.init()
        self.ingredients = ingredients
    }

    // MARK: - Public methods

    func fetchCocktails(completion: @escaping (Result<barFox.CocktailsResponse, Error>) -> Void) {
        if let drinks = drinks {
            completion(.success(drinks))
        } else {
            let error = NSError(domain: Constants.emptyText, code: 500, userInfo: nil)
            completion(.failure(error))
        }
    }

    func fetchIngridients(id: String, completion: @escaping (Result<barFox.IngridientsResponse, Error>) -> ()) {
        if let ingredients = ingredients {
            completion(.success(ingredients))
        } else {
            let error = NSError(domain: Constants.emptyText, code: 500, userInfo: nil)
            completion(.failure(error))
        }
    }
}
