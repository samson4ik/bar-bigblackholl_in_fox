// IngridientsViewController.swift
// Copyright © RoadMap. All rights reserved.

import UIKit

/// Экран со списком ингредиентов
final class IngridientsViewController: UITableViewController {
    // MARK: - Constants

    private enum Constants {
        static let emptyText = ""
        static let separator = ", "
        static let fatalErrorText = "init(coder:) has not been implemented"
        static let systemFontSize: CGFloat = 15
        static let igredientsCellID = "cellIngredients"
    }

    // MARK: - Private visual components

    private let ingridientsLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: Constants.systemFontSize)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 5
        label.textAlignment = .left
        label.lineBreakMode = .byTruncatingMiddle
        return label
    }()

    // MARK: - Public properties

    var drinkID: Optional = Constants.emptyText

    // MARK: - Private properties

    private let networkService: NetworkServicable?
    private let photoService: PhotoServicable?
    private var filteredIngredients: [String?] = []

    // MARK: - Init

    init(networkService: NetworkServicable?, photoService: PhotoServicable?, drinkID: String?) {
        self.networkService = networkService
        self.photoService = photoService
        self.drinkID = drinkID
        super.init(nibName: nil, bundle: nil)
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError(Constants.fatalErrorText)
    }

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setViewController()
    }

    // MARK: - Private methods

    private func setViewController() {
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: Constants.igredientsCellID)
        view.addSubview(ingridientsLabel)
        setConstaints()
        fetchIngredients()
    }

    private func fetchIngredients() {
        networkService?.fetchIngridients(id: drinkID ?? Constants.emptyText) { [weak self] result in
            guard let self else { return }
            switch result {
            case let .success(data):
                self.filterIngredients(ingredients: data)
            case let .failure(error):
                print(error.localizedDescription)
            }
        }
    }

    private func filterIngredients(ingredients: IngridientsResponse) {
        ingredients.drinks.forEach { item in
            filteredIngredients.append(item.ingredientOne)
            filteredIngredients.append(item.ingredientTwo)
            filteredIngredients.append(item.ingredientThree)
            filteredIngredients.append(item.ingredientFour)
            filteredIngredients.append(item.ingredientFive)
            filteredIngredients.append(item.ingredientSix)
            filteredIngredients.append(item.ingredientSeven)
            filteredIngredients.append(item.ingredientEight)
            filteredIngredients.append(item.ingredientTen)
        }
        DispatchQueue.main.async {
            let ingredientsMap = self.filteredIngredients.compactMap { $0 }
            self.filteredIngredients = ingredientsMap
            self.tableView.reloadData()
        }
    }

    private func setConstaints() {
        NSLayoutConstraint.activate([
            ingridientsLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            ingridientsLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            ingridientsLabel.widthAnchor.constraint(equalToConstant: 200),
            ingridientsLabel.heightAnchor.constraint(equalToConstant: 500)
        ])
    }
}

// MARK: - UITableViewDataSource

extension IngridientsViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        filteredIngredients.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.igredientsCellID)
        else { return UITableViewCell() }
        cell.textLabel?.text = filteredIngredients[indexPath.row]
        return cell
    }
}
